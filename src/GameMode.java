import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GameMode {

    private static JFrame gamemode = new JFrame("Modos de Jogo");
    private JLabel gamemodewindow = new JLabel();
    private JButton normal = new JButton("Normal");
    private JButton kitty = new JButton("Kitty");
    private JButton star = new JButton("Star");
    private ImageIcon wallpaper;

    public GameMode() {

        normal b1 = new normal();
        kitty b2 = new kitty();
        star b3 = new star();

        gamemode.setLayout(new BorderLayout());
        gamemode.setBounds(10, 10, 905, 700);
        gamemode.add(BorderLayout.CENTER, normal);
        gamemode.add(BorderLayout.CENTER, kitty);
        gamemode.add(BorderLayout.CENTER, star);
        gamemode.setResizable(false);
        gamemode.setLocationRelativeTo(null);
        gamemode.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        normal.setBounds(300, 240, 300, 50);
        normal.setBackground(new Color(48,44,43));
        normal.setFont(new Font("arial", Font.BOLD, 20));
        normal.setForeground(new Color(195, 55,54));
        normal.addActionListener(b1);

        kitty.setBounds(300, 300, 300, 50);
        kitty.setBackground(new Color(48,44,43));
        kitty.setFont(new Font("arial", Font.BOLD, 20));
        kitty.setForeground(new Color(195, 55,54));
        kitty.addActionListener(b2);

        star.setBounds(300, 360, 300, 50);
        star.setBackground(new Color(48,44,43));
        star.setFont(new Font("arial", Font.BOLD, 20));
        star.setForeground(new Color(195, 55,54));
        star.addActionListener(b3);


        wallpaper = new ImageIcon("img/wallpaper.jpg");

        gamemodewindow.setIcon(wallpaper);
        gamemode.add(gamemodewindow);
        gamemode.setVisible(true);
    }

    static class normal implements ActionListener {

        public void actionPerformed(ActionEvent play) {

            gamemode.dispose();

            JFrame normalMode = new JFrame();
            Gamepanel gameplay = new Gamepanel();

            normalMode.setBounds(10, 10, 905, 700);
            normalMode.setBackground(Color.DARK_GRAY);
            normalMode.setResizable(false);
            normalMode.setVisible(true);
            normalMode.setLocationRelativeTo(null);
            normalMode.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            normalMode.add(gameplay);
        }
    }

    static class kitty implements ActionListener {

        public void actionPerformed(ActionEvent quit) {

            gamemode.dispose();

            JFrame kittyMode = new JFrame();
            GamepanelKitty gameplay = new GamepanelKitty();

            kittyMode.setBounds(10, 10, 905, 700);
            kittyMode.setBackground(Color.DARK_GRAY);
            kittyMode.setResizable(false);
            kittyMode.setVisible(true);
            kittyMode.setLocationRelativeTo(null);
            kittyMode.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            kittyMode.add(gameplay);
        }
    }

    static class star implements ActionListener {

        public void actionPerformed(ActionEvent help) {

            gamemode.dispose();

            JFrame starMode = new JFrame();
            GamepanelStar gameplay = new GamepanelStar();

            starMode.setBounds(10, 10, 905, 700);
            starMode.setBackground(Color.DARK_GRAY);
            starMode.setResizable(false);
            starMode.setVisible(true);
            starMode.setLocationRelativeTo(null);
            starMode.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            starMode.add(gameplay);

        }
    }
}

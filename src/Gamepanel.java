import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

public class Gamepanel extends JPanel implements KeyListener, ActionListener {

    private int[] snakexlenght = new int [750];
    private int[] snakeylenght = new int [750];

    private int[] applexPos = {25, 50, 75, 100, 125, 150, 175, 200, 225, 250, 275, 300, 325, 350, 375, 400, 425, 450, 475, 500, 525, 550, 575, 600, 625, 650, 675, 700, 725, 750, 775, 800, 825, 850};
    private int[] appleyPos = {75, 100, 125, 150, 175, 200, 225, 250, 275, 300, 325, 350, 375, 400, 425, 450, 475, 500, 525, 550, 575, 600, 625};

    private int[] apple1xPos = {25, 50, 75, 100, 125, 150, 175, 200, 225, 250, 275, 300, 325, 350, 375, 400, 425, 450, 475, 500, 525, 550, 575, 600, 625, 650, 675, 700, 725, 750, 775, 800, 825, 850};
    private int[] apple1yPos = {75, 100, 125, 150, 175, 200, 225, 250, 275, 300, 325, 350, 375, 400, 425, 450, 475, 500, 525, 550, 575, 600, 625};

    private int[] applesKind = {1, 2 , 3};

    private int moves = 0;

    private Random random = new Random();

    private int xPos = random.nextInt(34);
    private int yPos = random.nextInt(23);
    private int xNewPos = random.nextInt(34); // Randomizador de outras maçãs
    private int yNewPos = random.nextInt(23); // Randomizador de outras maçãs
    private int apples = random.nextInt(3);

    private int score = 0;

    private boolean left = false;
    private boolean right = false;
    private boolean up = false;
    private boolean down = false;

    private ImageIcon rightMouth;
    private ImageIcon leftMouth;
    private ImageIcon upMouth;
    private ImageIcon downMouth;

    private ImageIcon appleImage;  // Maçã Simples
    private ImageIcon apple1Image; // Maçã Bomb
    private ImageIcon apple2Image; // Maçã Big fruit
    private ImageIcon apple3Image; // Maçã Decrease

    private int lenghtofSnake = 3;

    private ImageIcon snakeImage;

    private ImageIcon titleImage;

    private Timer timer;
    private int delay = 100;


    public Gamepanel() {

        addKeyListener(this);
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);
        timer = new Timer(delay, this);
        timer.start();
    }

    public void GameOver(Graphics g) {

        g.setColor(Color.RED);
        g.setFont(new Font("arial", Font.BOLD, 50));
        g.drawString("Game Over", 300, 300);

        g.setColor(Color.WHITE);
        g.setFont(new Font("arial", Font.BOLD, 20));
        g.drawString("Pressione ESPAÇO para recomeçar", 270, 340);

        g.setColor(Color.WHITE);
        g.setFont(new Font("arial", Font.BOLD, 20));
        g.drawString("Pressine ESC para voltar ao Menu", 275, 380);
    }

    public void paint(Graphics g) {

        if(moves == 0) {

            snakexlenght[2] = 50;
            snakexlenght[1] = 75;
            snakexlenght[0] = 100;

            snakeylenght[2] = 100;
            snakeylenght[1] = 100;
            snakeylenght[0] = 100;

        }

        // Desenha a borda da Imagem Título

        g.setColor(Color.WHITE);
        g.drawRect(24, 10, 851, 55);

        // Desenha a Imagem Título

        titleImage = new ImageIcon("img/snaketitle.jpg");
        titleImage.paintIcon(this, g, 25, 11);

        // Desenha a Borda do Mapa

        g.setColor(Color.WHITE);
        g.drawRect(24, 74, 851, 577);

        // Desenha o Background do Mapa

        g.setColor(Color.BLACK);
        g.fillRect(25,75, 850,575);

        // Desenha a Pontuação do jogo

        g.setColor(Color.WHITE);
        g.setFont(new Font("arial", Font.PLAIN, 14));
        g.drawString("Pontos: " + score, 680, 30);

        // Desenha o Tamanho da Cobra

        g.setColor(Color.WHITE);
        g.setFont(new Font("arial", Font.PLAIN, 14));
        g.drawString("Tamanho da Cobrinha: " + lenghtofSnake, 680, 50);

        rightMouth = new ImageIcon("img/rightmouth_corrigido.png");
        rightMouth.paintIcon(this, g, snakexlenght[0], snakeylenght[0]);

        for(int a = 0; a < lenghtofSnake; a++) {

            if(a == 0 && right) {

                rightMouth = new ImageIcon("img/rightmouth_corrigido.png");
                rightMouth.paintIcon(this, g, snakexlenght[a], snakeylenght[a]);
            }

            if(a == 0 && left) {

                leftMouth = new ImageIcon("img/leftmouth_corrigido.png");
                leftMouth.paintIcon(this, g, snakexlenght[a], snakeylenght[a]);
            }

            if(a == 0 && up) {

                upMouth = new ImageIcon("img/upmouth_corrigido.png");
                upMouth.paintIcon(this, g, snakexlenght[a], snakeylenght[a]);
            }

            if(a == 0 && down) {

                downMouth = new ImageIcon("img/downmouth_corrigido.png");
                downMouth.paintIcon(this, g, snakexlenght[a], snakeylenght[a]);
            }

            if(a != 0) {

                snakeImage = new ImageIcon("img/snakeimage.png");
                snakeImage.paintIcon(this, g, snakexlenght[a], snakeylenght[a]);

            }
        }

        appleImage = new ImageIcon("img/enemy.png");
        appleImage.paintIcon(this, g, applexPos[xPos], appleyPos[yPos]);


        if((applexPos[xPos] == snakexlenght[0] && appleyPos[yPos] == snakeylenght[0])) {

            score++;
            lenghtofSnake++;

            xPos = random.nextInt(34);
            yPos = random.nextInt(23);

            xNewPos = random.nextInt(34);
            yNewPos = random.nextInt(23);

            apples = random.nextInt(3);
        }

        if(xNewPos == xPos && yNewPos == yPos) {

            xNewPos = random.nextInt(34);
            yNewPos = random.nextInt(23);
        }

        if(score % 5 == 0 && score > 0) {

            if(applesKind[apples] == 1) {

                apple1Image = new ImageIcon("img/enemybomb.png");
                apple1Image.paintIcon(this, g, apple1xPos[xNewPos], apple1yPos[yNewPos]);

                if((apple1xPos[xNewPos] == snakexlenght[0] && apple1yPos[yNewPos] == snakeylenght[0])) {

                    right = false;
                    left = false;
                    up = false;
                    down = false;

                    g.setColor(Color.RED);
                    g.setFont(new Font("arial", Font.BOLD, 50));
                    g.drawString("Game Over", 300, 300);

                    g.setColor(Color.WHITE);
                    g.setFont(new Font("arial", Font.BOLD, 20));
                    g.drawString("Pressione ESPAÇO para recomeçar", 270, 340);

                    g.setColor(Color.WHITE);
                    g.setFont(new Font("arial", Font.BOLD, 20));
                    g.drawString("Pressine ESC para voltar ao menu", 275, 380);


                }

            }

            if(applesKind[apples] == 2) {

                apple2Image = new ImageIcon("img/enemybigfruit.png");
                apple2Image.paintIcon(this, g, apple1xPos[xNewPos], apple1yPos[yNewPos]);

                if((apple1xPos[xNewPos] == snakexlenght[0] && apple1yPos[yNewPos] == snakeylenght[0])) {

                    xNewPos = random.nextInt(34);
                    yNewPos = random.nextInt(23);

                    apples = random.nextInt(3);

                    score++;
                    score++;

                    lenghtofSnake++;

                }

            }

            if(applesKind[apples] == 3) {

                apple3Image = new ImageIcon("img/enemydecrease.png");
                apple3Image.paintIcon(this, g, apple1xPos[xNewPos], apple1yPos[yNewPos]);

                if((apple1xPos[xNewPos] == snakexlenght[0] && apple1yPos[yNewPos] == snakeylenght[0])) {

                    xNewPos = random.nextInt(34);
                    yNewPos = random.nextInt(23);

                    apples = random.nextInt(3);

                    lenghtofSnake--;

                }

            }

        }

        for(int b = 1; b < lenghtofSnake; b++) {

            if(snakexlenght[b] == snakexlenght[0] && snakeylenght[b] == snakeylenght[0]) {

                right = false;
                left = false;
                up = false;
                down = false;

                g.setColor(Color.RED);
                g.setFont(new Font("arial", Font.BOLD, 50));
                g.drawString("Game Over", 300, 300);

                g.setColor(Color.WHITE);
                g.setFont(new Font("arial", Font.BOLD, 20));
                g.drawString("Pressione ESPAÇO para recomeçar", 270, 340);

                g.setColor(Color.WHITE);
                g.setFont(new Font("arial", Font.BOLD, 20));
                g.drawString("Pressine ESC para voltar ao Menu", 275, 380);
            }
        }

        if((snakexlenght[0] >= 850) && (snakeylenght[0] >= 576)) {

            right = false;
            left = false;
            up = false;
            down = false;

            g.setColor(Color.RED);
            g.setFont(new Font("arial", Font.BOLD, 50));
            g.drawString("Game Over", 300, 300);

            g.setColor(Color.WHITE);
            g.setFont(new Font("arial", Font.BOLD, 20));
            g.drawString("Pressione ESPAÇO para recomeçar", 270, 340);

            g.setColor(Color.WHITE);
            g.setFont(new Font("arial", Font.BOLD, 20));
            g.drawString("Pressine ESC para voltar ao Menu", 275, 380);
        }

        g.dispose();
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        timer.start();

        try {

            Thread.sleep(1000/60);
        }

        catch(InterruptedException k) {

        }

        if(right) {

            for(int r = lenghtofSnake - 1; r >= 0; r--) {

                snakeylenght[r + 1] = snakeylenght[r];
            }

            for(int r = lenghtofSnake; r >= 0; r--) {

                if (r == 0) {

                    snakexlenght[r] = snakexlenght[r] + 25;
                }

                else {

                    snakexlenght[r] = snakexlenght[r-1];
                }

                if(snakexlenght[r] > 850) {

                    snakexlenght[r] = 850;
                }
            }

            repaint();
        }

        if(left) {

            for(int r = lenghtofSnake - 1; r >= 0; r--) {

                snakeylenght[r + 1] = snakeylenght[r];
            }

            for(int r = lenghtofSnake; r >= 0; r--) {

                if (r == 0) {

                    snakexlenght[r] = snakexlenght[r] - 25;
                }

                else {

                    snakexlenght[r] = snakexlenght[r-1];
                }

                if(snakexlenght[r] < 25) {

                    snakexlenght[r] = 25;
                }
            }

            repaint();
        }

        if(up) {

            for(int r = lenghtofSnake - 1; r >= 0; r--) {

                snakexlenght[r + 1] = snakexlenght[r];
            }

            for(int r = lenghtofSnake; r >= 0; r--) {

                if (r == 0) {

                    snakeylenght[r] = snakeylenght[r] - 25;
                }

                else {

                    snakeylenght[r] = snakeylenght[r-1];
                }

                if(snakeylenght[r] < 75) {

                    snakeylenght[r] = 75;
                }
            }

            repaint();
        }

        if(down) {

            for(int r = lenghtofSnake - 1; r >= 0; r--) {

                snakexlenght[r + 1] = snakexlenght[r];
            }

            for(int r = lenghtofSnake; r >= 0; r--) {

                if (r == 0) {

                    snakeylenght[r] = snakeylenght[r] + 25;
                }

                else {

                    snakeylenght[r] = snakeylenght[r-1];
                }

                if(snakeylenght[r] > 625) {

                    snakeylenght[r] = 625;
                }
            }

            repaint();
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

        if(e.getKeyCode() == KeyEvent.VK_SPACE) {

            moves = 0;
            score = 0;
            lenghtofSnake = 3;
            repaint();
        }

        if(e.getKeyCode() == KeyEvent.VK_ESCAPE) {

            System.gc();

            for (Window window : Window.getWindows()) {
                window.dispose();
            }

            Menu menu = new Menu();

        }

        if(e.getKeyCode() == KeyEvent.VK_RIGHT) {

            moves++;
            right = true;

            if(! left) {
                right = true;
            }

            else {

                right = false;
                left = true;
            }

            up = false;
            down = false;
        }

        if(moves != 0) {

            if (e.getKeyCode() == KeyEvent.VK_LEFT) {

                moves++;
                left = true;

                if (!right) {
                    left = true;
                } else {

                    left = false;
                    right = true;
                }

                up = false;
                down = false;
            }
        }

        if(e.getKeyCode() == KeyEvent.VK_UP) {

            moves++;
            up = true;

            if(! down) {
                up = true;
            }

            else {

                up = false;
                down = true;
            }

            left = false;
            right = false;
        }

        if(e.getKeyCode() == KeyEvent.VK_DOWN) {

            moves++;
            down = true;

            if(! up) {
                down = true;
            }

            else {

                up = true;
                down = true;
            }

            left = false;
            right = false;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}

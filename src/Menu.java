import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Menu {

    private static JFrame menu = new JFrame("Snake Game");
    private JLabel window = new JLabel();
    private JButton playButton = new JButton("Jogar");
    private JButton quitButton = new JButton("Sair");
    //private JButton helpButton = new JButton("Ajuda");
    private ImageIcon wallpaper;


    public Menu() {

        play b1 = new play();
        quit b2 = new quit();
        //help b3 = new help();

        menu.setLayout(new BorderLayout());
        menu.setBounds(10, 10, 905, 700);
        menu.add(BorderLayout.CENTER, playButton);
        menu.add(BorderLayout.CENTER, quitButton);
        //menu.add(BorderLayout.CENTER, helpButton);
        menu.setResizable(false);
        menu.setLocationRelativeTo(null);
        menu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        playButton.setBounds(300, 240, 300, 50);
        playButton.setBackground(new Color(48,44,43));
        playButton.setFont(new Font("arial", Font.BOLD, 20));
        playButton.setForeground(new Color(195, 55,54));
        playButton.addActionListener(b1);

        quitButton.setBounds(300, 300, 300, 50);
        quitButton.setBackground(new Color(48,44,43));
        quitButton.setFont(new Font("arial", Font.BOLD, 20));
        quitButton.setForeground(new Color(195,55,54));
        quitButton.addActionListener(b2);

        /*helpButton.setBounds(300, 360, 300, 50);
        helpButton.setBackground(new Color(48,44,43));
        helpButton.setFont(new Font("arial", Font.BOLD, 20));
        helpButton.setForeground(new Color(195,55,54));
        helpButton.addActionListener(b3);*/


        wallpaper = new ImageIcon("img/wallpaper.jpg");

        window.setIcon(wallpaper);
        menu.add(window);
        menu.setVisible(true);
    }

    static class play implements ActionListener {

        public void actionPerformed(ActionEvent play) {

            menu.dispose();

            GameMode gamemode = new GameMode();

        }
    }

    static class quit implements ActionListener {

        public void actionPerformed(ActionEvent quit) {

            System.exit(0);
        }
    }

    /*static class help implements ActionListener {

        public void actionPerformed(ActionEvent help) {

            //Frame obj = new JFrame();
            //Component ajuda = new Ajuda();

            //obj.add(ajuda);
        }
    }*/
}

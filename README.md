# EP2 "Jogo da Cobrinha" - Orientação a Objetos

- **Aluno:** Paulo Henrique Almeida da Silva
- **Matricula:** 17/0020291
- **Turma:** A - 195341
- **Data de entrega:** 12/11/2019

# Jogo

Jogo da Cobrinha com 3 tipos de cobras e 4 tipos de fruta diferentes.   

## Snakes

- **Normal:** A cobrinha não tem habilidades especiais. 
- **Kitty:** A cobrinha tem a habilidade de atravessar as bordas do jogo. 
- **Star:**: A cobrinha recebe pontos em dobro quando come frutas.   

## Frutas

- **Simple Fruit:** Aumenta os ponto em **1** quando comida.
- **Bomb Fruit:** Mata a cobrinha quando comida.
- **Big Fruit:** Aumenta os pontos em **2** quando comida.
- **Decrease Fruit:** Diminui o tamanho da cobrinha em **1** quando comida. 

## Java

Projeto feito no Intellij IDEA "IntelliJ IDEA 2019.1.3 (Ultimate Edition)"

- **JDK:** java version "11.0.5"
- **JRE:** openjdk version "1.8.0_181" /
           OpenJDK Runtime Environment (build 1.8.0_181-8u181-b13-2~deb9u1-b13) /
           OpenJDK 64-Bit Server VM (build 25.181-b13, mixed mode)


## Bibliotecas

1. Java swing 

2. Java awt

3. Java util 

## Execução
 
 Recomendo executar o projeto no Intellij, mas o programa também pode ser compilado e executado no terminal, desta maneira:
 
- **Compilar:**  javac Main.java
- **Executar:**  java Main


## Vunerabilidades do Projeto

É possível que ao compilar o código no eclipse ou o terminal do jogo fique sem imagens, dê preferência ao Intellij.
Tive um problema com o meu repósitório e tive que reforkar, aqui está o link do antigo com commits: https://gitlab.com/owhenrique/ep2-erro